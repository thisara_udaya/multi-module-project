# README #

This is a guide to create multi module application using maven.

### Maven commands ###

* mvn archetype:generate 
        -DgroupId=com.thisara 
        -Dpackagename=com.thisara 
        -DartifactId=my-studio 
        -DarchetypeArtifactId=maven-archetype-quickstart 
        -DinteractiveMode=false
        
* mvn archetype:generate 
        -DgroupId=com.thisara 
        -Dpackagename=com.thisara 
        -DartifactId=my-studio-photography 
        -DarchetypeArtifactId=maven-archetype-quickstart 
        -DinteractiveMode=false
        
* mvn archetype:generate 
        -DgroupId=com.thisara 
        -Dpackagename=com.thisara 
        -DartifactId=my-studio-videography 
        -DarchetypeArtifactId=maven-archetype-quickstart 
        -DinteractiveMode=false